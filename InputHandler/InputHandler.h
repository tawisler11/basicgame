#include <QKeyEvent>
#include <QMouseEvent>
#include <QObject>

#include "ui_MainWindow.h"

class InputHandler : public QObject
{
    Q_OBJECT
public:
    enum KeySetter
    {
        NONE,
        UP,
        DOWN,
        LEFT,
        RIGHT
    }setter;
    
private:
    Qt::Key moveUp;
    Qt::Key moveDown;
    Qt::Key moveLeft;
    Qt::Key moveRight;

    InputHandler():moveUp(Qt::Key_W),moveDown(Qt::Key_S),moveLeft(Qt::Key_A),moveRight(Qt::Key_D){};
    ~InputHandler(){};

    InputHandler(InputHandler const&);
    void operator=(const InputHandler& );
    
public:

    static InputHandler* getInstance(){static InputHandler instance; return &instance;};
    void configure();
    void handleKeyStroke(bool press, QKeyEvent *ke, Ui::MainWindow *ui);
    void handleKeyBinding(QKeyEvent *ke);

    void changeKeyRequest(KeySetter theSetter){ setter = theSetter; }

    inline Qt::Key getUpKey(){ return moveUp; }
    inline Qt::Key getDownKey(){ return moveDown; }
    inline Qt::Key getLeftKey(){ return moveLeft; }
    inline Qt::Key getRightKey(){ return moveRight; }
};
