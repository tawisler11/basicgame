#include "InputHandler.h"
#include "UiInterface/UiInterface.h"
#include "NetworkHandler/MessageGenerator.h"

void InputHandler::handleKeyStroke(bool press, QKeyEvent *ke, Ui::MainWindow *ui)
{
	if(ke->isAutoRepeat()) return;

	if(ke->key() == moveUp)
    {
        MessageGenerator::getInstance()->writeMoveUp(press);
    }
    if(ke->key() == moveDown)
    {
        MessageGenerator::getInstance()->writeMoveDown(press);
    }
    if(ke->key() == moveLeft)
    {
        MessageGenerator::getInstance()->writeMoveLeft(press);
    }
    if(ke->key() == moveRight)
    {
        MessageGenerator::getInstance()->writeMoveRight(press);
    }
    else if(press && ((ke->key() == Qt::Key_Enter) || (ke->key() == Qt::Key_Return)))
    {
        QString message = ui->sayLine->text().trimmed();
        if(!message.isEmpty())
        {
            MessageGenerator::getInstance()->writeMessage(QString(message));
        }
        ui->sayLine->clear();
    }
}

void InputHandler::handleKeyBinding(QKeyEvent *ke)
{
	switch(setter)
	{
		case NONE:
			break;
        case UP:
        	moveUp = static_cast<Qt::Key>(ke->key());
        	UiInterface::getInstance()->setUpButtonText(QKeySequence(moveUp).toString());
        	break;

        case DOWN:
        	moveDown = static_cast<Qt::Key>(ke->key());
        	UiInterface::getInstance()->setDownButtonText(QKeySequence(moveDown).toString());
        	break;

        case LEFT:
        	moveLeft = static_cast<Qt::Key>(ke->key());
        	UiInterface::getInstance()->setLeftButtonText(QKeySequence(moveLeft).toString());
        	break;

        case RIGHT:
        	moveRight = static_cast<Qt::Key>(ke->key());
        	UiInterface::getInstance()->setRightButtonText(QKeySequence(moveRight).toString());
        	break;

        default:
        	break;
	}

	setter = NONE;
}