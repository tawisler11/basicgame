#include <QKeyEvent>
#include <QMouseEvent>
#include <QTcpSocket>
#include <QObject>

#include "ui_MainWindow.h"

class UiInterface : public QObject
{
    Q_OBJECT

private:

    Ui::MainWindow *ui;

    UiInterface(){};
    ~UiInterface(){};

    UiInterface(UiInterface const&);
    void operator=(const UiInterface& );
    
public:

    static UiInterface* getInstance(){static UiInterface instance; return &instance;};

    inline void setUi(Ui::MainWindow *ui){ this->ui = ui;}

    inline void setUpButtonText(QString text){ ui->upButton->setText(text); }
    inline void setDownButtonText(QString text){ ui->downButton->setText(text); }
    inline void setLeftButtonText(QString text){ ui->leftButton->setText(text); }
    inline void setRightButtonText(QString text){ ui->rightButton->setText(text); }

};
