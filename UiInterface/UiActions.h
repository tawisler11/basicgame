#ifndef UIACTIONS_H
#define UIACTIONS_H

namespace UiActions
{
	enum Actions
	{
		DO_NOTHING = 0,
		SET_LOGIN_PAGE,
		SET_GAME_PAGE,
		SEND_CHAT_MESSAGE,
		CLEAR_CHAT
	};
}

#endif