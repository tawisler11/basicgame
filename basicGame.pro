#-------------------------------------------------
#
# Project created by QtCreator 2015-07-03T00:48:25
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = basicGame
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp\
        InputHandler/InputHandler.cpp\
        NetworkHandler/NetworkHandler.cpp\
        NetworkHandler/MessageGenerator.cpp\

HEADERS  += mainwindow.h\
		 Player.h\
		 InputHandler/InputHandler.h\
		 NetworkHandler/NetworkHandler.h\
		 NetworkHandler/MessageGenerator.h\
		 NetworkHandler/ReceivedMessage.h\
		 UiInterface/UiInterface.h\

FORMS    += mainwindow.ui
