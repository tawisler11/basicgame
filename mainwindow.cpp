#include "MainWindow.h"

#include <QCryptographicHash>
#include <QColor>
#include <QGraphicsEllipseItem>
#include <QGraphicsTextItem>
#include <QMessageBox>
#include <QDir>
#include <QSize>
#include <QKeySequence>


#include "InputHandler/InputHandler.h"
#include "NetworkHandler/MessageGenerator.h"
#include "UiInterface/UiInterface.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    setupUi(this);

    networkThread = new QThread();
    networkHandler = new NetworkHandler();
    networkHandler->moveToThread(networkThread);
    
    //connections
    connect(this, SIGNAL(connectSocket(QString)), networkHandler, SLOT(connectToSocket(QString)));
    connect(this, SIGNAL(disconnectSocket()), networkHandler, SLOT(disconnectFromSocket()));
    connect(networkHandler, SIGNAL(connectedToSocket()), this, SLOT(connected()));
    connect(networkHandler, SIGNAL(disconnectedFromSocket()), this, SLOT(disconnected()));

    connect(networkHandler, SIGNAL(sendMessageFromServer(ReceivedMessage)),
            this, SLOT(handleMessageFromServer(ReceivedMessage)));
    connect(MessageGenerator::getInstance(), SIGNAL(sendMessageToServer(QString)),
            networkHandler, SLOT(transmitMessage(QString)));

    networkThread->start();

    QPixmap up(QDir::currentPath() + "/res/up.png");
    QPixmap down(QDir::currentPath() + "/res/down.png");
    QPixmap left(QDir::currentPath() + "/res/left.png");
    QPixmap right(QDir::currentPath() + "/res/right.png");

    QSize size(50,50);

    upLabel->setPixmap(up.scaled(size));
    downLabel->setPixmap(down.scaled(size));
    leftLabel->setPixmap(left.scaled(size));
    rightLabel->setPixmap(right.scaled(size));

    upButton->setText(QKeySequence(InputHandler::getInstance()->getUpKey()).toString());
    downButton->setText(QKeySequence(InputHandler::getInstance()->getDownKey()).toString());
    leftButton->setText(QKeySequence(InputHandler::getInstance()->getLeftKey()).toString());
    rightButton->setText(QKeySequence(InputHandler::getInstance()->getRightKey()).toString());

    uiAction(UiActions::SET_LOGIN_PAGE);

    UiInterface::getInstance()->setUi(this);

    timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(fps()));

    moveTimer = new QTimer();
    connect(moveTimer, SIGNAL(timeout()), this, SLOT(move()));
}

void MainWindow::move()
{
    foreach(Player* player, players)
        {
            
        }
}

void MainWindow::fps()
{
    update();
}

void MainWindow::paintEvent(QPaintEvent *)
{
    if(stackedWidget->currentWidget() == gamePage)
    {
        //textBox->append(QString::number(players.size()));

        QGraphicsScene *scene;
        scene = new QGraphicsScene(this);
        scene->setSceneRect(0,0,931,541);
        graphics->setScene(scene);

        QBrush brush(Qt::red);
        QPen pen(Qt::black);
        pen.setWidth(1);

        foreach(Player* player, players)
        {
            brush.setColor(player->getColor());
            scene->addEllipse(player->getLocation().first, player->getLocation().second, 20, 20, pen, brush);
        }
    }
    else if(stackedWidget->currentWidget() == creationPage)
    {
        QGraphicsScene *scene;
        scene = new QGraphicsScene(this);
        scene->setSceneRect(0,0,30,30);
        preview->setScene(scene);

        QBrush brush(Qt::red);
        QPen pen(Qt::black);
        pen.setWidth(1);

        QColor color(redSelect->sliderPosition(),greenSelect->sliderPosition(),blueSelect->sliderPosition());
        brush.setColor(color);
        scene->addEllipse(4, 4, 20, 20, pen, brush);
    }
}


void MainWindow::on_exitSettingsButton_clicked()
{
    stackedWidget->setCurrentWidget(loginPage);
}

void MainWindow::on_settingsButton_clicked()
{
    stackedWidget->setCurrentWidget(settingsPage);
}

void MainWindow::on_keyBindingsButton_clicked()
{
    settingsStackedWidget->setCurrentWidget(keyBindingsPage);
}

void MainWindow::on_graphicsButton_clicked()
{
    settingsStackedWidget->setCurrentWidget(graphicsPage);
}

void MainWindow::on_audioButton_clicked()
{
    settingsStackedWidget->setCurrentWidget(audioPage);
}

void MainWindow::on_upButton_clicked()
{
    InputHandler::getInstance()->changeKeyRequest(InputHandler::UP);
}

void MainWindow::on_downButton_clicked()
{
    InputHandler::getInstance()->changeKeyRequest(InputHandler::DOWN);
}

void MainWindow::on_leftButton_clicked()
{
    InputHandler::getInstance()->changeKeyRequest(InputHandler::LEFT);
}

void MainWindow::on_rightButton_clicked()
{
    InputHandler::getInstance()->changeKeyRequest(InputHandler::RIGHT);
}

void MainWindow::on_loginButton_clicked()
{
    qDebug() << "starting to connect";

    loginText->setText("Attempting to connect...");

    qApp->processEvents();
    
    emit connectSocket(serverAddress->text());
}

void MainWindow::on_disconnectButton_clicked()
{
    emit disconnectSocket();
}

void MainWindow::on_sayButton_clicked()
{
    uiAction(UiActions::SEND_CHAT_MESSAGE);
}

void MainWindow::on_submitCreationButton_clicked()
{
    MessageGenerator::getInstance()->writeJoinGame(nameBox->text(), QString::number(redSelect->sliderPosition()),
        QString::number(greenSelect->sliderPosition()), QString::number(blueSelect->sliderPosition()));
}
void MainWindow::on_availableButton_clicked()
{
    MessageGenerator::getInstance()->writeAvailableQuery(nameBox->text());
    availability->setText("Waiting on server...");
    availability->setStyleSheet("QLabel { color : black; }");
}

// This function gets called when our socket has successfully connected to the chat
// server. (see the connect() call in the MainWindow constructor).
void MainWindow::connected()
{
    stackedWidget->setCurrentWidget(creationPage);

    timer->start(1000/60);
}

void MainWindow::disconnected()
{
    sayLine->clear();
    textBox->clear();

    foreach(Player *player, players)
    {
        delete player;
        players.remove(player);
    }

    stackedWidget->setCurrentWidget(loginPage);

    timer->stop();

    QMessageBox::information(this, "Lost Connection", "You have been disconnected.");
}

void MainWindow::mousePressEvent(QMouseEvent *me)
{
    //socket->write(QString("mouse press\n").toUtf8());
}
void MainWindow::mouseReleaseEvent(QMouseEvent *me)
{
    //socket->write(QString("mouse release\n").toUtf8());
}
void MainWindow::keyPressEvent(QKeyEvent *ke)
{
    if(stackedWidget->currentWidget() == settingsPage)
    {
        InputHandler::getInstance()->handleKeyBinding(ke);
    }
    InputHandler::getInstance()->handleKeyStroke(true, ke, this);
}
void MainWindow::keyReleaseEvent(QKeyEvent *ke)
{
    InputHandler::getInstance()->handleKeyStroke(false, ke, this);
}

void MainWindow::uiAction(UiActions::Actions action)
{
    switch(action)
    {
        case UiActions::DO_NOTHING:
            //How the fuck did you get here? Pls.
            break;
        case UiActions::SET_LOGIN_PAGE:

            stackedWidget->setCurrentWidget(loginPage);
            break;

        case UiActions::SET_GAME_PAGE:

            stackedWidget->setCurrentWidget(gamePage);
            break;

        case UiActions::SEND_CHAT_MESSAGE:
        {
            QString message = sayLine->text().trimmed();
            if(!message.isEmpty())
            {
                MessageGenerator::getInstance()->writeMessage(message);
            }
            sayLine->clear();
            break;
        }
        case UiActions::CLEAR_CHAT:
            sayLine->clear();
            break;
    }
}


void MainWindow::handleMessageFromServer(ReceivedMessage rcvMsg)
{
    if(rcvMsg.id == MessageId::AVAILABILITY)
    {
        availabilityResponse(*reinterpret_cast<bool*>(rcvMsg.data));
        delete rcvMsg.data;
    }
    else if(rcvMsg.id == MessageId::NEW_PLAYER)
    {
        newPlayer(reinterpret_cast<Player*>(rcvMsg.data));
    }
    else if(rcvMsg.id == MessageId::DISCONNECTED)
    {
        playerLeft(*reinterpret_cast<QString*>(rcvMsg.data));
        delete rcvMsg.data;
    }
    else if(rcvMsg.id == MessageId::PLAYER_MOVED)
    {
        playerMoved(reinterpret_cast<Player*>(rcvMsg.data));
        delete rcvMsg.data;
    }
    else if(rcvMsg.id == MessageId::MESSAGE)
    {
        incommingMessage(*reinterpret_cast<QString*>(rcvMsg.data));
        delete rcvMsg.data;
    }
    else if(rcvMsg.id == MessageId::USERS)
    {
        usersList(*reinterpret_cast<QStringList*>(rcvMsg.data));
        delete rcvMsg.data;
    }
    else if(rcvMsg.id == MessageId::ACCEPTED)
    {
        acceptedResponse(*reinterpret_cast<bool*>(rcvMsg.data));
        delete rcvMsg.data;
    }
}

void MainWindow::availabilityResponse(bool theAvailability)
{
    if(theAvailability)
    {
        availability->setText("Available!");
        availability->setStyleSheet("QLabel { color : green; }");
    }
    else
    {
        availability->setText("Not Available");
        availability->setStyleSheet("QLabel { color : red; }");
    }
}

void MainWindow::newPlayer(Player* thePlayer)
{
    players.insert(thePlayer);
}

void MainWindow::playerLeft(QString leaver)
{
    Player *player = NULL;
    foreach(Player* thePlayer, players)
    {
        if(thePlayer->getUsername() == leaver)
        {
            player = thePlayer;
            break;
        }
    }

    if(player)
    {
        players.remove(player);
        delete player;
        player = NULL;
    }
}

void MainWindow::playerMoved(Player* movedPlayer)
{
    Player* player;
    foreach(Player* thePlayer, players)
    {
        if(thePlayer->getUsername() == movedPlayer->getUsername())
        {
            player = thePlayer;
            break;
        }
    }

    if(player)
    {
        player->setLocation(movedPlayer->getLocation());
    }
}

void MainWindow::incommingMessage(QString message)
{
    if(!message.isEmpty()) textBox->append(message);
}

void MainWindow::usersList(QStringList users)
{
    userListWidget->clear();

    foreach(QString user, users)
        new QListWidgetItem(QPixmap(":/user.png"), user, userListWidget);
}

void MainWindow::acceptedResponse(bool accepted)
{
    if(accepted)
    {
        uiAction(UiActions::SET_GAME_PAGE);
    }
    else
    {
        availability->setText("Not Available");
        availability->setStyleSheet("QLabel { color : red; }");
    }
}