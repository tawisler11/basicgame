#include <QObject>
#include <QString>

#ifndef MessageGenerator_H
#define MessageGenerator_H

class MessageGenerator : public QObject
{
	Q_OBJECT
	MessageGenerator(){};
    ~MessageGenerator(){};

    MessageGenerator(MessageGenerator const&);
    void operator=(const MessageGenerator& );
    
public:
    static MessageGenerator* getInstance(){static MessageGenerator instance; return &instance;};

    void writeJoinGame(QString name, QString red, QString green, QString blue);
    void writeAvailableQuery(QString name);
    void writeMessage(QString theMessage);
    void writeMoveUp(bool enable);
    void writeMoveDown(bool enable);
    void writeMoveLeft(bool enable);
    void writeMoveRight(bool enable);

signals:
	void sendMessageToServer(QString messageToSend);

};

#endif