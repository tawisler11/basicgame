#include "NetworkHandler.h"

NetworkHandler::NetworkHandler()
{
	playersListRegex = QRegExp("^/players:(.*)$");
	playerListRegex = QRegExp("^/player:(.*)$");
	disconnectedRegex = QRegExp("^/disconnected:(.*)$");
	locationRegex = QRegExp("^([^:]+):x(.*),y(.*)$");
	messageRegex = QRegExp("^([^:]+):(.*)$");
	usersRegex = QRegExp("^/users:(.*)$");
	availabilityRegex = QRegExp("^/availablility:(.*)$");
	acceptedRegex = QRegExp("^/accepted:(.*)$");

	socket = new QTcpSocket(this);

	connect(socket, SIGNAL(readyRead()), this, SLOT(readyRead()));
	connect(socket, SIGNAL(connected()), this, SLOT(connected()));
	connect(socket, SIGNAL(disconnected()), this, SLOT(disconnected()));
}

NetworkHandler::~NetworkHandler()
{
}


void NetworkHandler::readyRead()
{
    while(socket->canReadLine())
    {
        QString line = QString::fromUtf8(socket->readLine()).trimmed();

        MessageTypes messageType = parseRawType(line);

        switch(messageType)
        {
            case ERROR:
                break;

            case PLAYERS:
            {
                parsePlayers(line);
                break;
            }   
            case PLAYER:
            {
                parsePlayer(line);
                break;
            }   
            case DISCONNECTED:
            {
                parseDisconnect(line);
                break;
            }    
            case LOCATION:
            {
                parseLocation(line);
                break;
            }    
            case MESSAGE:
            {
                parseMessage(line);
                break;
            }    
            case USERS:
            {
                parseUsers(line);
                break;
            }    
            case AVAILABILITY:
            {
                parseAvailability(line);
                break;
            }    
            case ACCEPTED:
            {
                parseAccepted(line);
                break;
            }    
            default:
                break;
                
        }

    }
}

void NetworkHandler::connectToSocket(QString serverAddress)
{
	socket->connectToHost(serverAddress, 4200);
}

void NetworkHandler::disconnectFromSocket()
{
	socket->disconnectFromHost();
}

//==========================================================
//				WRITE MESSAGES TO SERVER
//==========================================================

void NetworkHandler::transmitMessage(QString message)
{
	socket->write(message.toUtf8());
}

// This function gets called when our socket has successfully connected to the chat
// server. (see the connect() call in the MainWindow constructor).
void NetworkHandler::connected()
{
    emit connectedToSocket();
}

void NetworkHandler::disconnected()
{
    emit disconnectedFromSocket();
}

NetworkHandler::MessageTypes NetworkHandler::parseRawType(QString theMessage)
{
	MessageTypes retval = ERROR;

	if(playersListRegex.indexIn(theMessage) != -1)
	{
		retval = PLAYERS;
	}
	else if(playerListRegex.indexIn(theMessage) != -1)
	{
		retval = PLAYER;
	}
	else if(disconnectedRegex.indexIn(theMessage) != -1)
	{
		retval = DISCONNECTED;
	}
	else if(usersRegex.indexIn(theMessage) != -1)
	{
		retval = USERS;
	}
	else if(availabilityRegex.indexIn(theMessage) != -1)
	{
		retval = AVAILABILITY;
	}
	else if(acceptedRegex.indexIn(theMessage) != -1)
	{
		retval = ACCEPTED;
	}
	else if(locationRegex.indexIn(theMessage) != -1)
	{
		retval = LOCATION;
	}
	else if(messageRegex.indexIn(theMessage) != -1)
	{
		retval = MESSAGE;
	}

	return retval;
}

bool NetworkHandler::parsePlayers(QString theMessage)
{
	bool retval = false;

	if(playersListRegex.indexIn(theMessage) != -1)
	{
		QStringList currentPlayers = playersListRegex.cap(1).split(":");
	    QStringList attributes;

	    foreach(QString currentPlayer, currentPlayers)
	    {
	        if(!currentPlayer.isEmpty())
	        {
	            attributes = currentPlayer.split(",");
	            Player* player = new Player();
	            player->setUsername(attributes.takeFirst());

	            QPair<int,int> location;
	            location.first = attributes.takeFirst().toInt();
	            location.second = attributes.takeFirst().toInt();
	            player->setLocation(location);

	            QColor color(attributes.takeFirst().toInt(),
	                attributes.takeFirst().toInt(),attributes.takeFirst().toInt());
	            player->setColor(color);

	            ReceivedMessage rcvMsg;
	            rcvMsg.id = MessageId::NEW_PLAYER;
	            rcvMsg.numBytes = sizeof(Player);
	            rcvMsg.data = reinterpret_cast<unsigned*>(player);
	            emit sendMessageFromServer(rcvMsg);
	        }
	    }

	    retval = true;
	}

	return retval;
}

bool NetworkHandler::parsePlayer(QString theMessage)
{
	bool retval = false;

	if(playerListRegex.indexIn(theMessage) != -1)
    {
        QString theNewPlayer = playerListRegex.cap(1);
        QStringList attributes;

        attributes = theNewPlayer.split(",");
        Player* player = new Player();
        player->setUsername(attributes.takeFirst());

        QPair<int,int> location;
        location.first = attributes.takeFirst().toInt();
        location.second = attributes.takeFirst().toInt();
        player->setLocation(location);

        unsigned int red = attributes.takeFirst().toInt();
        unsigned int green = attributes.takeFirst().toInt();
        unsigned int blue = attributes.takeFirst().toInt();

        QColor color(red,green,blue);
        player->setColor(color);

    	ReceivedMessage rcvMsg;
        rcvMsg.id = MessageId::NEW_PLAYER;
        rcvMsg.numBytes = sizeof(Player);
        rcvMsg.data = reinterpret_cast<unsigned*>(player);
        emit sendMessageFromServer(rcvMsg);

        retval = true;
    }

    return retval;
}

bool NetworkHandler::parseDisconnect(QString theMessage)
{
	bool retval = false;

	QString disconnectedPlayer;
	if(disconnectedRegex.indexIn(theMessage) != -1)
    {
        disconnectedPlayer = disconnectedRegex.cap(1);

    	ReceivedMessage rcvMsg;
        rcvMsg.id = MessageId::DISCONNECTED;
        rcvMsg.numBytes = sizeof(disconnectedPlayer.toUtf8().size());
        rcvMsg.data = reinterpret_cast<unsigned*>(new QString(disconnectedPlayer));
        emit sendMessageFromServer(rcvMsg);

        retval = true;
    }

    return retval;
}

bool NetworkHandler::parseLocation(QString theMessage)
{
	bool retval = false;

	Player* player = new Player();
	if(locationRegex.indexIn(theMessage) != -1) //user moves ball here
    {
        //investigate better way to do this
        QString user = locationRegex.cap(1);
        player->setUsername(user);
        
        QString x = locationRegex.cap(2);
        QString y = locationRegex.cap(3);

        QPair<int,int> location;
        location.first = x.toInt();
        location.second = y.toInt();
        player->setLocation(location);

    	ReceivedMessage rcvMsg;
        rcvMsg.id = MessageId::PLAYER_MOVED;
        rcvMsg.numBytes = sizeof(Player);
        rcvMsg.data = reinterpret_cast<unsigned*>(player);
        emit sendMessageFromServer(rcvMsg);

        retval = true;
    }

    return retval;
}

bool NetworkHandler::parseMessage(QString theMessage)
{
	bool retval = false;

	QString message;
	if(messageRegex.indexIn(theMessage) != -1)
    {
        QString user = messageRegex.cap(1);
        QString text = messageRegex.cap(2);

        message = QString("<b>" + user + "</b>: " + text);

    	ReceivedMessage rcvMsg;
        rcvMsg.id = MessageId::MESSAGE;
        rcvMsg.numBytes = sizeof(message.toUtf8().size());
        rcvMsg.data = reinterpret_cast<unsigned*>(new QString(message));
        emit sendMessageFromServer(rcvMsg);

        retval = true;
    }

    return retval;
}

bool NetworkHandler::parseUsers(QString theMessage)
{
	bool retval = false;

	QStringList users;
	if(usersRegex.indexIn(theMessage) != -1)
    {
        users = usersRegex.cap(1).split(",");

    	ReceivedMessage rcvMsg;
        rcvMsg.id = MessageId::USERS;
        rcvMsg.numBytes = sizeof(users);
        rcvMsg.data = reinterpret_cast<unsigned*>(new QStringList(users));
        emit sendMessageFromServer(rcvMsg);

        retval = true;
    }

    return retval;
}

bool NetworkHandler::parseAvailability(QString theMessage)
{
	bool retval = false;

	bool* availability = new bool();
	if(availabilityRegex.indexIn(theMessage) != -1)
    {
		QString theAvailability = availabilityRegex.cap(1);
	    if(theAvailability == "true")
	    {
	    	*availability = true;
	    }
	    else
	    {
	    	*availability = false;	
	    }

	    ReceivedMessage rcvdMsg;
	    rcvdMsg.id = MessageId::AVAILABILITY;
	    rcvdMsg.numBytes = sizeof(bool);
	    rcvdMsg.data = reinterpret_cast<unsigned*>(availability);
	    emit sendMessageFromServer(rcvdMsg);

	    retval = true;
	}

	return retval;
}

bool NetworkHandler::parseAccepted(QString theMessage)
{
	bool retval = false;

	bool* accepted = new bool();
	if(acceptedRegex.indexIn(theMessage) != -1)
    {
		QString theAccepted = acceptedRegex.cap(1);
	    if(theAccepted == "true")
	    {
	    	*accepted = true;
	    }
	    else
	    {
	    	*accepted = false;	
	    }

	    //emit acceptedResponse(accepted);
	    ReceivedMessage rcvdMsg;
	    rcvdMsg.id = MessageId::ACCEPTED;
	    rcvdMsg.numBytes = sizeof(bool);
	    rcvdMsg.data = reinterpret_cast<unsigned*>(accepted);

	    emit sendMessageFromServer(rcvdMsg);

	    retval = true;
	}

	return retval;
}