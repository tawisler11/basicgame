#include "MessageId.h"

#ifndef ReceivedMessage_H
#define ReceivedMessage_H

struct ReceivedMessage
{
   MessageId::MessageId id;
   unsigned numBytes;
   unsigned* data;
};

#endif