#include <QSet>
#include <QString>
#include <QRegExp>
#include <QObject>
#include <QTcpSocket>

#include "Player.h"
#include "ReceivedMessage.h"
#include "..\MessageId.h"
#include "ui_MainWindow.h"

#ifndef NetworkHandler_H
#define NetworkHandler_H

class NetworkHandler : public QObject
{
    Q_OBJECT
public:
    enum MessageTypes
    {
        ERROR,
        PLAYERS,
        PLAYER,
        DISCONNECTED,
        LOCATION,
        MESSAGE,
        USERS,
        AVAILABILITY,
        ACCEPTED
    };
    
private:
    QTcpSocket *socket;

    QRegExp playersListRegex;
    QRegExp playerListRegex;
    QRegExp disconnectedRegex;
    QRegExp locationRegex;
    QRegExp messageRegex;
    QRegExp usersRegex;
    QRegExp availabilityRegex;
    QRegExp acceptedRegex;

public:
    NetworkHandler();
    ~NetworkHandler();

    MessageTypes parseRawType(QString theMessage);

    bool parsePlayers(QString theMessage);
    bool parsePlayer(QString theMessage);
    bool parseDisconnect(QString theMessage);
    bool parseLocation(QString theMessage);
    bool parseMessage(QString theMessage);
    bool parseUsers(QString theMessage);
    bool parseAvailability(QString theMessage);
    bool parseAccepted(QString theMessage);

private slots:
    void connected();
    void disconnected();
    void readyRead();

public slots:
    void connectToSocket(QString serverAddress);
    void disconnectFromSocket();
    void transmitMessage(QString message);

signals:
    void connectedToSocket();
    void disconnectedFromSocket();
    void sendMessageFromServer(ReceivedMessage rcvMsg);
};

#endif