#include "MessageGenerator.h"

void MessageGenerator::writeJoinGame(QString name, QString red, QString green, QString blue)
{
	QString message = QString("/me:" + name + ":r" + red + ":g" + green + ":b" + blue + "\n");
	emit sendMessageToServer(message);
}

void MessageGenerator::writeAvailableQuery(QString name)
{
	QString message = QString("/available:" + name + "\n");
	emit sendMessageToServer(message);
}

void MessageGenerator::writeMessage(QString theMessage)
{
	QString message = QString(theMessage + "\n");
	emit sendMessageToServer(message);
}

void MessageGenerator::writeMoveUp(bool enable)
{
	QString message = enable ? QString("/move:up true\n") : QString("/move:up false\n");
	emit sendMessageToServer(message);
}

void MessageGenerator::writeMoveDown(bool enable)
{
	QString message = enable ? QString("/move:down true\n") : QString("/move:down false\n");
	emit sendMessageToServer(message);
}

void MessageGenerator::writeMoveLeft(bool enable)
{
	QString message = enable ? QString("/move:left true\n") : QString("/move:left false\n");
	emit sendMessageToServer(message);
}

void MessageGenerator::writeMoveRight(bool enable)
{
	QString message = enable ? QString("/move:right true\n") : QString("/move:right false\n");
	emit sendMessageToServer(message);
}