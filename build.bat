@echo off
mkdir ..\build\
mkdir ..\..\deliverables\
mkdir ..\..\deliverables\basicGameClientDebug
mkdir ..\..\deliverables\basicGameClientDebug\res

pushd ..\build\
qmake ..\src\basicGame.pro
mingw32-make -f MakeFile.Debug clean
mingw32-make -f MakeFile.Debug
pushd debug
xcopy basicGame.exe ..\..\..\deliverables\basicGameClientDebug\ /y
xcopy ..\..\res ..\..\..\deliverables\basicGameClientDebug\res\ /S/E/Y
popd
popd
