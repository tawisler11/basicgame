#ifndef MessageId_H
#define MessageId_H

namespace MessageId
{
	enum MessageId
	{
	   SOCKET_CONNECTED,
	   SOCKET_DISCONNECTED,
	   NEW_PLAYER,
	   PLAYER_MOVED,
	   DISCONNECTED,
	   LOCATION,
	   MESSAGE,
	   USERS,
	   AVAILABILITY,
	   ACCEPTED
	};
}

#endif