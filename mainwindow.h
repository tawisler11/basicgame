#include <QMainWindow>
#include <QKeyEvent>
#include <QPainter>
#include <QMap>
#include <QString>
#include <QPair>
#include <QTimer>
#include <QThread>

#include "ui_MainWindow.h"

#include "Player.h"
#include "NetworkHandler/NetworkHandler.h"
#include "NetworkHandler/ReceivedMessage.h"

#include "UiInterface/UiActions.h"

class MainWindow : public QMainWindow, public Ui::MainWindow
{
    Q_OBJECT

    public:

        MainWindow(QWidget *parent=0);
        void paintEvent(QPaintEvent *);
        void mousePressEvent(QMouseEvent *me);
        void mouseReleaseEvent(QMouseEvent *me);
        void keyPressEvent(QKeyEvent *ke);
        void keyReleaseEvent(QKeyEvent *ke);

    private slots:

        void on_exitSettingsButton_clicked();
        void on_settingsButton_clicked();
        void on_keyBindingsButton_clicked();
        void on_graphicsButton_clicked();
        void on_audioButton_clicked();
        void on_upButton_clicked();
        void on_downButton_clicked();
        void on_leftButton_clicked();
        void on_rightButton_clicked();
        void on_loginButton_clicked();
        void on_sayButton_clicked();
        void on_disconnectButton_clicked();
        void on_submitCreationButton_clicked();
        void on_availableButton_clicked();
        

    public slots:
        void fps();
        void move();
        void connected();
        void disconnected();

        void handleMessageFromServer(ReceivedMessage rcvMsg);

    signals:
        void connectSocket(QString serverAddress);
        void disconnectSocket();

    private:

        QTimer *timer;
        QTimer *moveTimer;

        QSet<Player*> players;

        QThread *networkThread;
        NetworkHandler *networkHandler;

        void uiAction(UiActions::Actions action);
        void availabilityResponse(bool theAvailability);
        void newPlayer(Player* thePlayer);
        void playerLeft(QString leaver);
        void playerMoved(Player* movedPlayer);
        void incommingMessage(QString message);
        void usersList(QStringList users);
        void acceptedResponse(bool accepted);
};
