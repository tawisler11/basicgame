#include "mainwindow.h"
#include <QApplication>
#include "NetworkHandler/ReceivedMessage.h"
#include "Player.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    qRegisterMetaType<ReceivedMessage>("ReceivedMessage");
	qRegisterMetaType<Player>("Player");

    MainWindow w;
    w.show();

    return a.exec();
}
