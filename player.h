#include <QString>
#include <QPair>
#include <QColor>

#ifndef Player_H
#define Player_H

class Player
{
    QString username;
    QPair<int,int> location;
    QColor color;

public:
    Player():username(""),location(QPair<int,int>(0,0)),color(QColor()){};
    ~Player(){};

    QString getUsername(){return username;}
    QPair<int,int> getLocation(){return location;}
    QColor getColor(){return color;}

    void setUsername(QString username){this->username = username;}
    void setLocation(QPair<int,int> location){this->location = location;}
    void setColor(QColor color){this->color = color;}
};

#endif